from pages.resources_page import ResourcePage
from tests.base_test import BaseTest
from pages.user_account import SignIn


class TestSignIn(BaseTest):

    def setUp(self):
        super(TestSignIn, self).setUp()
        resource = ResourcePage(self.driver)
        resource.open()


    def test_sign_in(self):
        users = SignIn(self.driver)
        users.sign_in()
        self.assertTrue("admin" in str(self.driver.current_url))

    def test_sign_out(self):
        users = SignIn(self.driver)
        users.sign_in()
        users.sign_out()
        self.assertTrue("admin" not in str(self.driver.current_url))

    def test_register_new_user(self):
        users = SignIn(self.driver)
        self.assertTrue(users.registration_new_user())