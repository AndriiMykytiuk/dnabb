from pages.virtual_reference_page import VirtualReferencePage
from tests.base_test import BaseTest

class VirtualReferenceTest(BaseTest):

    def setUp(self):
        super(VirtualReferenceTest, self).setUp()
        users = VirtualReferencePage(self.driver)
        users.open()

    def test_virtual_reference_button_is_shown(self):
        virtual_ref = VirtualReferencePage(self.driver)
        self.assertTrue(virtual_ref.virtual_reference_button_is_shown())

    def test_virtual_regerence_button_is_clickable(self):
        virtual_ref = VirtualReferencePage(self.driver)
        virtual_ref.virtual_reference_button_click()
        self.assertTrue('faq' in str(self.driver.current_url))

    def test_ask_question(self):
        virtual_ref = VirtualReferencePage(self.driver)
        virtual_ref.virtual_reference_button_click()
        self.assertTrue(virtual_ref.ask_question())

    def test_choose_category_dropdown(self):
        virtual_ref = VirtualReferencePage(self.driver)
        virtual_ref.virtual_reference_button_click()
        virtual_ref.faq_search()
        self.assertTrue('results' in str(self.driver.current_url()))
