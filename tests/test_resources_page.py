from pages.resources_page import ResourcePage
from tests.base_test import BaseTest

class ResursesTest(BaseTest):

    # Testing if Resources button available on desktop
    def test_is_shown(self):
        resources = ResourcePage(self.driver)
        resources.open()
        self.assertTrue(resources.is_shown())

    # Tests is Resources button is clickable
    def test_is_clickable(self):
        resources = ResourcePage(self.driver)
        resources.open()
        resources.is_clickable()
        self.assertTrue("resources" in str(self.driver.current_url))

    # Checks if 10 pictures available in new arrivals post
    def test_new_arrivals_pictures(self):
        resources = ResourcePage(self.driver)
        resources.open()
        self.assertEqual(resources.pictures_is_available(), 10)

    # Checks if title of preview and a title of opened page in new arrivals are equals
    def test_correct_article_is_opened(self):
        resources = ResourcePage(self.driver)
        resources.open()
        self.assertTrue(resources.correct_article_is_opened())

