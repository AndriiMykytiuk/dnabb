from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from pages.base_page import BasePage
from .resources_page import ResourcePage
from selenium.webdriver.support.ui import Select
from random import randint


class VirtualReferencePage(BasePage):

    def virtual_reference_button_click(self):
        virtual_reference_button = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located((By.XPATH, '//*[contains(@href, "/faq")]'))
        )
        virtual_reference_button.click()

    def virtual_reference_button_is_shown(self):
        resources = ResourcePage(self.driver)
        try:
            virtual_reference_button = WebDriverWait(self.driver, 35).until(
                EC.presence_of_element_located((By.XPATH, '//*[contains(@href, "/faq")]'))
            )
            return resources.is_shown(virtual_reference_button)
        except:
            raise Exception ("Virtual reference button not found!")

    def ask_question(self):

        name = "Nikita Dzhigurda"
        mail = "abc@gmail.com"
        question = "Who is Zabolotny. Can you advice a literature?"

        name_field = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located((By.XPATH, '//*[contains(@placeholder, "прізвище")]'))
        )
        name_field.click()
        name_field.send_keys(name)

        email_field = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located((By.XPATH, '//*[contains(@placeholder, "E-mail")]'))
        )
        email_field.click()
        email_field.send_keys(mail)

        question_field = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located((By.XPATH, '//*[contains(@placeholder, "Напишіть нам")]'))
        )
        question_field.click()
        question_field.send_keys(question)

        send_question_field = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located((By.XPATH, '//*[contains(@value, "Відправити питання")]'))
        )

        send_question_field.click()

        question_approve = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located((By.XPATH, '//*[contains(text(), "Дякуємо за запитання!")]'))
        )

        return question_approve.is_displayed()

    def faq_search(self):
        choose_category_dropdown = self.driver.find_element_by_xpath('//*[@id="edit_field_faq_section_tid_chosen"]/a')
        choose_category_dropdown.click()
        all_options = WebDriverWait(self.driver, 35).until(
            EC.presence_of_all_elements_located((By.XPATH, '//*[@class="active-result"]'))
        )
        rand_option = all_options[randint(1, len(all_options) - 1)]
        rand_option.click()
        submit_button = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="edit-submit-faq"]'))
        )
        submit_button.click()